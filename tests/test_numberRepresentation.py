from unittest import TestCase
from allyourbase.standard_number import StandardNumber
from decimal import *

__author__ = 'david'


class TestNumberRepresentation(TestCase):

    def test_clean_up_number_string_positive_whole(self):
        numrep = StandardNumber(10)
        (i, f) = numrep.clean_up_number_string("1234")
        self.assertTrue(numrep.positive)
        self.assertEqual("1234", i)
        self.assertEqual("", f)

    def test_clean_up_number_string_positive_fraction(self):
        numrep = StandardNumber(10)
        (i, f) = numrep.clean_up_number_string(".412332")
        self.assertTrue(numrep.positive)
        self.assertEqual("0", i)
        self.assertEqual("412332", f)

    def test_clean_up_number_string_positive_mixed(self):
        numrep = StandardNumber(10)
        (i, f) = numrep.clean_up_number_string("523523.432")
        self.assertTrue(numrep.positive)
        self.assertEqual("523523", i)
        self.assertEqual("432", f)

    def test_clean_up_number_string_negative_whole(self):
        numrep = StandardNumber(10)
        (i, f) = numrep.clean_up_number_string("-1234")
        self.assertFalse(numrep.positive)
        self.assertEqual("1234", i)
        self.assertEqual("", f)

    def test_clean_up_number_string_negative_fraction(self):
        numrep = StandardNumber(10)
        (i, f) = numrep.clean_up_number_string("-.412332")
        self.assertFalse(numrep.positive)
        self.assertEqual("", i)
        self.assertEqual("412332", f)

    def test_clean_up_number_string_negative_mixed(self):
        numrep = StandardNumber(10)
        (i, f) = numrep.clean_up_number_string("-523523.432")
        self.assertFalse(numrep.positive)
        self.assertEqual("523523", i)
        self.assertEqual("432", f)

    def test_clean_up_number_positive_whole(self):
        numrep = StandardNumber(10)
        (i, f) = numrep.clean_up_number(Decimal('1234'))
        self.assertTrue(numrep.positive)
        self.assertAlmostEqual(Decimal('1234'), i, places=0)
        self.assertAlmostEqual(Decimal('0'), f, places=0)

    def test_clean_up_number_positive_fraction(self):
        numrep = StandardNumber(10)
        (i, f) = numrep.clean_up_number(Decimal('0.412332'))
        self.assertTrue(numrep.positive)
        self.assertAlmostEqual(Decimal('0'), i, places=0)
        self.assertAlmostEqual(Decimal('0.412332'), f, places=0)

    def test_clean_up_number_positive_mixed(self):
        numrep = StandardNumber(10)
        (i, f) = numrep.clean_up_number(Decimal('523523.432'))
        self.assertTrue(numrep.positive)
        self.assertAlmostEqual(Decimal('523523'), i, places=0)
        self.assertAlmostEqual(Decimal('0.432'), f, places=0)

    def test_clean_up_number_negative_whole(self):
        numrep = StandardNumber(10)
        (i, f) = numrep.clean_up_number(Decimal('-1234'))
        self.assertFalse(numrep.positive)
        self.assertAlmostEqual(Decimal('1234'), i, places=0)
        self.assertAlmostEqual(Decimal('0'), f, places=0)

    def test_clean_up_number_negative_fraction(self):
        numrep = StandardNumber(10)
        (i, f) = numrep.clean_up_number(Decimal('-.412332'))
        self.assertFalse(numrep.positive)
        self.assertAlmostEqual(Decimal('0'), i, places=0)
        self.assertAlmostEqual(Decimal('0.412332'), f, places=0)

    def test_clean_up_number_negative_mixed(self):
        numrep = StandardNumber(10)
        (i, f) = numrep.clean_up_number(Decimal('-523523.432'))
        self.assertFalse(numrep.positive)
        self.assertAlmostEqual(Decimal('523523'), i, places=0)
        self.assertAlmostEqual(Decimal('0.432'), f, places=0)
